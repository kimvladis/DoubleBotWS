var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var withdrawAllSchema = new Schema({
  assetids: [],
  sum: Number,
  createdAt: { type: Date }
});

module.exports = mongoose.model('WithdrawAll', withdrawAllSchema);