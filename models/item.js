var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var itemSchema = new Schema({
  assetid: {type: String, unique: true},
  name: String,
  img: String,
  price: Number,
  reject: String,
  botid: Number,
  view: String
});

module.exports = mongoose.model('Item', itemSchema);