var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var doubleSchema = new Schema({
  id: Number,
  name: String,
  image: String,
  opskins: String,
  price: Number,
  reject: Boolean,
  updatedAt: Date,
  lastSeenAt: Date
});

module.exports = mongoose.model('Double', doubleSchema);