var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var withdrawSchema = new Schema({
  assetids: [],
  sum: Number,
  createdAt: { type: Date, expires: 60 * 60 }
});

module.exports = mongoose.model('Withdraw', withdrawSchema);