var redisPub = require('./redisPub');
var socket   = require('./socket');

var bots = [];

function count () {
  return bots.reduce(
    function (prev, cur) {
      return prev + (cur.isReady ? 1 : 0);
    }, 0
  );
}

function countAll () {
  return bots.length;
}

module.exports.notify = function (_socket, isReady) {
  var bot = bots.find(
    function (bot) {
      return bot.id === _socket.id;
    }
  );
  if ( !bot ) {
    bot = _socket;
    bots.push(bot);
  }
  bot.isReady = isReady;
  var _count   = count();
  redisPub.publish('botCount', _count);
  socket.emit('botsCount', _count + '/' + countAll());
};

module.exports.remove = function (_socket) {
  var index = bots.findIndex(
    function (bot) {
      return bot.id === _socket.id;
    }
  );

  if (index > -1) {
    bots.splice(index, 1);
  }
};

module.exports.countAll = countAll;

module.exports.get = function () {
  return bots.find(
    function (bot) {
      return bot.isReady;
    }
  );
};

module.exports.count = count;