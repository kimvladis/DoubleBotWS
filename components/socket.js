var io             = require('socket.io');
var redisClient    = require('./redis');
var Withdraw       = require('../models/withdraw');
var bots           = require('./bots');
var holdHandler    = require('../handlers/hold');
var releaseHandler = require('../handlers/release');
var socket;

module.exports.listen = function (app) {
  socket = io(app);

  socket.on(
    'connection', function (socket) {
      redisClient.smembers(
        'held', function (err, data) {
          if ( data ) {
            socket.emit('held', { ids: data });
          }
        }
      );
      socket.emit('botsCount', bots.count() + '/' + bots.countAll());
      socket.emit('lastWithdraw', global.lastWithdraw);

      Withdraw.find(
        function (err, items) {
          if ( err ) {
            return console.error(err);
          }

          socket.emit(
            'lastHour',
            items.reduce(
              function (prev, curr) {
                return prev + curr.sum;
              }, 0
            )
          );
        }
      );

      socket.on(
        'disconnect', function () {
          bots.notify(socket, false);
          bots.remove(socket);
        }
      );

      socket.on(
        'hold', function (data) {
          var id = data.id;

          holdHandler(id);
        }
      );

      socket.on(
        'release', function (data) {
          var id = data.id;

          releaseHandler(id);
        }
      );

      socket.on(
        'ready', function (data) {
          bots.notify(socket, data);
        }
      );
    }
  );

  return socket;
};

module.exports.emit = function (chanel, message) {
  if (socket) {
    socket.emit(chanel, message);
  }
  else {
    setTimeout(function () {
      socket.emit(chanel, message);
    }, 5000)
  }
};
