var redis    = require('redis');
var redisSub = redis.createClient();
var redisClient = require('./redis');
var bots     = require('./bots');
var socket   = require('./socket');
var redisPub = require('./redisPub');
var Withdraw = require('../models/withdraw');
var logs       = require('./logger');

var logger = logs.getRedisLogger;

redisSub.subscribe('withdraw');
redisSub.subscribe('reqBotCount');
redisSub.subscribe('chart');
redisSub.subscribe('captchasCount');
redisSub.subscribe('balance');
redisSub.subscribe('captchaBalance');

redisSub.on(
  "message", function (channel, message) {
    logger.trace(channel + ' - ' + message);
    message = JSON.parse(message);
    switch ( channel ) {
      case "withdraw":
        var bot = bots.get();

        if ( bot ) {
          bot.isReady = false;
          bot.emit('withdraw', message);
        }

        message.time = new Date();
        socket.emit('lastWithdraw', message);
        global.lastWithdraw = message;

        Withdraw.find(
          function (err, items) {
            if ( err ) {
              return console.error(err);
            }

            socket.emit(
              'lastHour',
              items.reduce(
                function (prev, curr) {
                  return prev + curr.sum;
                }, 0
              )
            );
          }
        );

        break;
      case "reqBotCount":
        redisPub.publish('botCount', bots.count());

        break;
      case "chart":
        socket.emit('chart', message);

        break;
      case "captchasCount":
        socket.emit('captchasCount', message);

        break;
      case "balance":
        redisClient.set('balance', JSON.stringify(message));
        socket.emit('balance', message);

        break;
      case "captchaBalance":
        socket.emit('captchaBalance', message);

        break;
    }
  }
);

module.exports = redisSub;