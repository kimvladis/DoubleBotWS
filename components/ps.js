/**
 * Process sync module
 * @type {{stack: boolean, lock_status: boolean, lock: module.exports.lock, queue: Array, process: module.exports.process, unlock: module.exports.unlock}}
 */
module.exports = {
  lock_status:false,
  lock:function(){
    console.log('lock');
    this.lock_status=true;
  },
  queue:[],
  process:function(callback,argument){
    if(this.lock_status==false){
      console.log('process');
      callback.apply(this,argument);
    }
    else
    {
      console.log('queue');
      this.queue.push([callback,argument]);
    }
  },
  unlock:function(){
    console.log('unlock');
    this.lock_status=false;

    if(this.queue.length>0)
    {
      console.log('unqueue');
      var p = this.queue.shift();
      this.process(p[0],p[1]);
    }
  }
};