var log4js = require('log4js');

log4js.configure({
                   appenders: [
                     {
                       type: 'file',
                       filename: "logs/redis.log",
                       category: 'redis',
                       maxLogSize: 20000000,
                       backups: 10
                     },
                     {
                       type: 'file',
                       filename: "logs/app.log",
                       category: 'app',
                       maxLogSize: 2000000,
                       backups: 10
                     },
                     {
                       type: 'file',
                       filename: "logs/tor.log",
                       category: 'tor',
                       maxLogSize: 2000000,
                       backups: 10
                     },
                     {
                       type: 'file',
                       filename: "logs/captcha.log",
                       category: 'captcha',
                       maxLogSize: 2000000,
                       backups: 10
                     },
                     {
                       type: 'console',
                       category: 'redis'
                     },
                     {
                       type: 'console',
                       category: 'app'
                     },
                     {
                       type: 'console',
                       category: 'tor'
                     },
                     {
                       type: 'console',
                       category: 'captcha'
                     }
                   ]
                 });

module.exports.getAppLogger     = log4js.getLogger('app');
module.exports.getCaptchaLogger = log4js.getLogger('captcha');
module.exports.getTorLogger     = log4js.getLogger('tor');
module.exports.getRedisLogger   = log4js.getLogger('redis');