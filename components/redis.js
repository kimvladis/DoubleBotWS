var redis      = require('redis');
var client     = redis.createClient();
var logs       = require('./logger');

var logger = logs.getRedisLogger;

client.on(
  "error", function (err) {
    logger.error("Error " + err);
  }
);

module.exports = client;