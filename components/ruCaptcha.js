var request  = require('request');
var logs     = require('./logger');
var redis    = require('./redis');
var red      = require('redis');
var redisPub = red.createClient();

var logger = logs.getCaptchaLogger;

var loading = false;

module.exports = {
  siteKey:      "6Le5ciQTAAAAAOvknIBwQa-SFZaeHGpwLsUqofc9",
  ruCaptchaKey: "a22a2df1d1a1fae2b2b6fcd87e83aceb",
  proxy:        "77.244.213.220:3128",
  pageUrl:      "http://csgopolygon.com/withdraw.php",
  captchaIds:   [],
  errorList:    ['ERROR_WRONG_ID_FORMAT', 'ERROR_WRONG_CAPTCHA_ID'],

  send: function (data, cb) {
    data.key = this.ruCaptchaKey;
    data.json = 1;

    request(
      {
        url:    "http://rucaptcha.com/res.php",
        method: "GET",
        qs:     data,
        json:   true
      }, function (error, response, body) {
        cb(body);
      }
    );
  },
  loadOne: function (cb) {
    if (!loading) {
      loading = true;

      request(
        {
          url:    "http://rucaptcha.com/in.php",
          method: "POST",
          form:   {
            'key':       this.ruCaptchaKey,
            'method':    "userrecaptcha",
            'googlekey': this.siteKey,
            'proxy':     this.proxy,
            'proxytype': "HTTP",
            'pageurl':   this.pageUrl,
            'json':      1
          },
          json:   true
        }, function (error, response, body) {
          redis.lpush('captchas', body.request, function (err, data) {
            loading = false;
            if (cb)
              this.getOne(cb);
          }.bind(this));
        }.bind(this)
      );
    }
  },
  getOne:  function (cb) {
    redis.rpop('captchas', function (err, data) {
      if (data) {
        var captchaId = data;

        this.send(
          {
            'action': 'get',
            'id':     captchaId
          },
          function (body) {
            if (body && body.status == 1) {
              // console.log(body.request);
              cb(body.request);
            } else {
              logger.trace(body);
              if (this.errorList.indexOf(body.request) < 0) {
                redis.lpush('captchas', captchaId, function (err, data) {
                  setTimeout(function () {
                    this.getOne(cb);
                  }.bind(this), 3000);
                }.bind(this));
              } else {
                this.getOne(cb);
              }
            }
          }.bind(this)
        );
      } else {
        this.loadOne(cb);
      }
    }.bind(this));
  },
  getBalance: function (cb) {
    this.send({'action': 'getbalance'}, function (data) {
      if (data && data.status == 1) {
        cb(data.request);
      } else {
        logger.trace(data);
      }
    });
  },
  fillPull: function () {
    redis.llen('captchas', function (err, data) {
      logger.trace('current pull - ' + data);
      redisPub.publish('captchasCount', data);
      if (data < 5) {
        logger.trace('getting one...');
        this.loadOne(false);
      }
      setTimeout(this.fillPull.bind(this), 1000);
    }.bind(this));
    // redis.rpop('captchas', function (err, data) {
    //   console.log(err, data);
    // });
    // redis.lpush('captchas', 1, function (err, data) {
    //   console.log(err, data);
    // });
    // if (this.captchaIds.length < 5) {
    //   logger.trace('getting one. current pull - ' + this.captchaIds.length);
    //   this.getOne(false);
    // }
    //
    // setTimeout(this.fillPull.bind(this), 1000);
  },
  checkBalance: function () {
    this.getBalance(function (data) {
      redisPub.publish('captchaBalance', data);

      setTimeout(this.checkBalance.bind(this), 60 * 1000);
    }.bind(this))
  }
};