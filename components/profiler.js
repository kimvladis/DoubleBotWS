var
  countStart = [],
  counters = {};

module.exports.start = function(name) {
  var hrTime = process.hrtime();

  if (name === undefined) {
    countStart.push(hrTime);
  } else {
    counters[name] = hrTime;
  }
};

module.exports.stop = function(name) {

  var countdown;

  if (name === undefined) {
    countdown = process.hrtime(countStart.pop());
  } else {
    countdown = process.hrtime(counters[name]);
  }

  return ((countdown[0] * 1e9 + countdown[1]) / 1e6).toFixed(2);
};