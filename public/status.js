moment.locale('ru');

var socket = io();

var charts = {};

$('.epoch').each(function () {
  $this = $(this);
  charts[$this.attr('id')] = $this.epoch(
    {
      type: 'time.bar',
      data: [
        {
          label: $this.attr('id'),
          values: [ ]
        }
      ]
    }
  );
});

socket.on(
  'chart', function (data) {
    console.log(data);

    charts[data.type].push(
      [
        {
          time: data.time,
          y: data.y
        }
      ]
    );
  }
);

socket.on(
  'botsCount', function (data) {
    $('#botsCount').text(data);
  }
);

socket.on(
  'lastWithdraw', function (data) {
    $('#withdrawSum').text(data.sum);
    $('#withdrawTime').text(moment(data.time).fromNow());
  }
);

socket.on(
  'lastHour', function (data) {
    $('#lastHour').text(data);
  }
);

socket.on(
  'captchasCount', function (data) {
    $('#captchasCount').text(data);
  }
);

socket.on(
  'balance', function (data) {
    $('#balance').text(data.balance);
    $('#availiable').text(data.avail);
  }
);


socket.on(
  'captchaBalance', function (data) {
    $('#captchaBalance').text(data / 0.160);
  }
);
