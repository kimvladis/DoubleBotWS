Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

function get(callback) {
  $.get('/bot/list/get', callback);
}

function set(data, callback) {
  $.post('/bot/list/set', data, callback);
}

var $items = $('#items');

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  get(function (result) {
      repaintItems(result.allItems, result.items);
  });
}
function utf8_to_b64(str) {
  return window.btoa(unescape(encodeURIComponent(str)));
}
function b64_to_utf8(str) {
  return decodeURIComponent(escape(window.atob(str)));
}
function getItem(item, isOn) {
  return "<li class='list-group-item row'>" +
    "<input type='checkbox' class='item-checkbox' " + (isOn ? 'checked' : '') + ">" +
    "<span class='item-name'>" + item + "</span>" +
    "<button data-val='" + utf8_to_b64(item) + "' class='remove_item btn btn-default pull-right'>-</button></li>";
}
function removeItemHandler() {
  const item = b64_to_utf8($(this).data('val'));
  get(function (result) {
    var allItems = result.allItems;
    var items = result.items;
    allItems.remove(item + '');
    items.remove(item + '');
    set({items: items, allItems: allItems}, function () {
      onChanged();
    });
  });
}
function addItemHandler() {
  const item = $('#item').val();
  get(function (result) {
    var allItems = result.allItems;
    var items = result.items;
    items.remove(item);
    items.push(item);
    allItems.remove(item);
    allItems.push(item);
    set({items: items, allItems: allItems}, function (data) {
      onChanged();
    });
  });
  return false;
}
function clearHandler () {
  set({items: [], allItems: []}, function (data) {
    onChanged();
  });
}
function repaintItems(allItems, items) {
  $items.html(allItems.reduce(function (prev, curr) {
    var isOn = $.inArray(curr, items) >= 0;
    return prev += getItem(curr, isOn);
  }, ''))
}
function onChanged () {
  get(function (result) {
    repaintItems(result.allItems, result.items);
  });
}

$(document).on('click', '.remove_item', removeItemHandler);
$(document).on('click', '#add_item', addItemHandler);
$(document).on('click', '#clear', clearHandler);
// $(document).on('click', '#save', save_options);
document.addEventListener('DOMContentLoaded', restore_options);

$(function () {
  $(document).on('change', '.item-checkbox', function () {
    var allItems = $('#items').find('.item-name').map(function (i, curr, arr) {
      return $(curr).text();
    }).get();
    var items = $('#items').find('input:checked').map(function (i, curr, arr) {
      return $(curr).next().text();
    }).get();

    set({items: items, allItems: allItems});
  });
  $("#items").sortable();
  $("#items").disableSelection();
  $("#items").on('sortupdate', function () {
    var allItems = $('#items').find('.item-name').map(function (i, curr, arr) {
      return $(curr).text();
    }).get();
    var items = $('#items').find('input:checked').map(function (i, curr, arr) {
      return $(curr).next().text();
    }).get();

    set({items: items, allItems: allItems});
  });
  var $fileChooser = $('input[type=file]');
  $fileChooser.bootstrapFileInput();
  $fileChooser.on('change', function (evt) {
    var f = evt.target.files[0];
    if (f) {
      var reader = new FileReader();
      reader.onload = function (e) {
        var contents = e.target.result;
        var importItems = contents.split("\n");
        if (importItems[0] != "CSGODOUBLE_BOT_IMPORT") {
          alert('Unsupported file type');
          return false;
        } else {
          importItems.shift();
        }
        get( function (result) {
          var allItems = result.allItems;
          var items = result.items;

          importItems.forEach(function (item, i, arr) {
            if (item != "") {
              items.remove(item);
              items.push(item);
              allItems.remove(item);
              allItems.push(item);
            }
          });

          set({items: items, allItems: allItems}, onChanged);
        });
      };
      reader.readAsText(f);
    }
  });
});