/**
 * curl 'http://tryskins.ru/web/site/items2'
 * -H 'Accept-Encoding: gzip, deflate, sdch'
 *
 * -H 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
 * -H 'Upgrade-Insecure-Requests: 1'
 * -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116
 * Safari/537.36'
 * -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp;q=0.8'
 * -H 'Referer: http://tryskins.ru/web/site/items2'
 * -H 'Cookie: __cfduid=dd00f8d9edaef070a643af5a92234b8101473748356;
 * _csrf=9955db658bf1e1cdeb5cf4ae002a3a94e215f7e8b3803ccba8b8551e3859c646a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22-sp-1IjL_6ZAcZzW3LugPK0r8ul9lu2K%22%3B%7D;
 * _ym_uid=1473748357861700661; _ym_isad=1; PHPSESSID=17k58tuefaumujeiutt98vids4;
 * cf_clearance=58ddce56b87544a8d7264df7fb6046aee7466386-1475119815-300'
 * -H 'Connection: keep-alive' --compressed
 */
process.env.DEBUG = true;

var
  Horseman = require('node-horseman'),
  redisPub = require('./components/redisPub'),
  redisCli = require('./components/redis'),
  profiler = require('./components/profiler'),
  cookies  = require('./components/cookies');


var horseman = new Horseman({
    timeout: 100000
  }
);

var url = 'https://tryskins.ru';

horseman
  .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36")
  .headers(
    {
      "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4",
      "Upgrade-Insecure-Requests": 1,
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Connection": "keep-alive"
    }
  )
  .viewport(1280,800)
  .cookies(cookies)
  .open("https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=http%3A%2F%2Ftryskins.ru%2Fweb%2Flogin%2Fsteam&openid.realm=http%3A%2F%2Ftryskins.ru&openid.ns.sreg=http%3A%2F%2Fopenid.net%2Fextensions%2Fsreg%2F1.1&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select")
  .screenshot('2.png')
  .reload()
  .wait(1000)
  .click('#imageLogin')
  .wait(1000)
  .waitForNextPage()
  .waitForNextPage()
  // .screenshot('4.png')
  // .waitForNextPage()
  // .open('http://tryskins.ru/web/login/steam?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=id_res&openid.op_endpoint=https%3A%2F%2Fsteamcommunity.com%2Fopenid%2Flogin&openid.claimed_id=http%3A%2F%2Fsteamcommunity.com%2Fopenid%2Fid%2F76561197984477142&openid.identity=http%3A%2F%2Fsteamcommunity.com%2Fopenid%2Fid%2F76561197984477142&openid.return_to=http%3A%2F%2Ftryskins.ru%2Fweb%2Flogin%2Fsteam&openid.response_nonce=2016-10-11T06%3A28%3A32ZvO9bHexaXVppbRLU9kLq9QyujOM%3D&openid.assoc_handle=1234567890&openid.signed=signed%2Cop_endpoint%2Cclaimed_id%2Cidentity%2Creturn_to%2Cresponse_nonce%2Cassoc_handle&openid.sig=seB%2BDgFaWHB2Cjnvc%2FOTg1qvj7o%3D')
  // .screenshot('4.png')
  .wait(5000)
  .screenshot('5.png')
  .evaluate(function () {
    return $('table tr').map(function () {
      var resp = {};
      resp[$(this).find('td:nth-child(1)').text()] = $(this).find('td:nth-child(2)').text();
      return resp;
    }).get();
  })
  .then(function (items) {
    console.log(items);
    horseman.close();
  });

//
// var Horseman = require("node-horseman");
// var horseman = new Horseman();
//
// horseman
//   .open('http://lite.yelp.com/search?find_desc=pizza&find_loc=94040&find_submit=Search')
//   .text('address')
//   .then(function(address){
//     console.log(address);
//     horseman.close();
//   });