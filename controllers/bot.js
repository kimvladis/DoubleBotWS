var express        = require('express');
var redisClient    = require('../components/redis');
var ps             = require('../components/ps.js');
var holdHandler    = require('../handlers/hold');
var releaseHandler = require('../handlers/release');
var socket         = require('../components/socket');

var router = express.Router();


router.get(
  '/check/:id', function (req, res) {
    var id = req.params.id;
    ps.process(
      function () {
        ps.lock();
        redisClient.sismember(
          'held', id, function (err, data) {
            if ( data ) {
              ps.unlock();
              res.json({ held: true });
            } else {
              redisClient.sadd(
                'held', id, function () {
                  socket.emit('held', { ids: [ id ] });
                  ps.unlock();
                  res.json({ held: false });
                }
              );
            }
          }
        );
      }
    );
  }
);

router.get(
  '/hold/:id', function (req, res) {
    var id = req.params.id;

    holdHandler(
      id, function () {
        res.json({ held: true });
      }
    );
  }
);

router.get(
  '/release/:id', function (req, res) {
    var id = req.params.id;

    releaseHandler(
      id, function () {
        res.json({ released: true });
      }
    );
  }
);

module.exports = router;