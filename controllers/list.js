var express     = require('express');
var redisClient = require('../components/redis');

var router = express.Router();

router.get(
  '/', function (req, res) {
    res.render('list.html');
  }
);

router.get(
  '/get', function (req, res) {
    redisClient.get(
      'list', function (err, data) {
        data = JSON.parse(data);
        if ( data && data.hasOwnProperty('items') ) {
          res.json(data);
        } else {
          var response = { items: [], allItems: [] };
          redisClient.set(
            'list', JSON.stringify(response), function () {
              res.json(response);
            }
          );
        }
      }
    );
  }
);

router.post(
  '/set', function (req, res) {
    redisClient.set(
      'list', JSON.stringify(req.body), function () {
        res.json({ success: true });
      }
    );
  }
);

module.exports = router;