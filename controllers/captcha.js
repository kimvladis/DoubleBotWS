var express  = require('express');
var router   = express.Router();
var redis    = require('redis');
var redisPub = redis.createClient();


router.get(
  '/', function (req, res) {
    res.render('index.html');
  }
);

router.get(
  '/:captcha', function (req, res) {
    var captcha = req.params.captcha;

    redisPub.publish("captcha", captcha);
    res.json({ success: true });
  }
);

module.exports = router;