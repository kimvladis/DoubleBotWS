var express  = require('express');
var router   = express.Router();

router.get(
  '/', function (req, res) {
    res.render('status.html');
  }
);

module.exports = router;