var TelegramBot = require('node-telegram-bot-api');
var redisCli = require('./components/redis');
var redisSub = require('./components/redisPub');

var token = '244553341:AAHbM1CykBAL99rIk4VAqCEY-ivKlpcbntM';
var reidsKey = 'telegramClients';
// Setup polling way
var bot = new TelegramBot(token, {polling: true});
const me = 189445022;
var justSended = false;

// Matches /auth [pass]
bot.onText(/\/auth (.+)/, function (msg, match) {
  var fromId = msg.from.id;
  var resp = match[1];
  if (resp == 'charlie') {
    redisCli.sadd(
      reidsKey,
      fromId,
      function (err, data) {
        console.log(err, data);
        bot.sendMessage(fromId, 'success');
      }
    )
  } else {
    bot.sendMessage(fromId, 'error');
  }
});

// Matches /captcha [solve]
bot.onText(/\/captcha (.+)/, function (msg, match) {
  var fromId = msg.from.id;
  var resp = match[1];

  redisCli.set(
    'captcha',
    resp,
    function (err, data) {
      console.log(err, data);
      bot.sendMessage(fromId, 'success');
    }
  )
});

// Matches /logout [whatever]
bot.onText(/\/logout/, function (msg, match) {
  var fromId = msg.from.id;
  redisCli.srem(
    reidsKey,
    fromId,
    function (err, data) {
      bot.sendMessage(fromId, 'success');
    }
  );
});

bot.on('message', function (msg) {
  if (justSended) {
    justSended = false;

    redisCli.set(
      'captcha',
      msg.text,
      function (err, data) {
        console.log(err, data);
        bot.sendMessage(me, 'success');
      }
    )
  }
});

function say (message) {
  redisCli.smembers(
    reidsKey,
    function (err, data) {
      console.log(data);
      if (data) {
        data.forEach(function (client) {
          bot.sendMessage(client, message);
        })
      }
    }
  );
}

function sayMe (message) {
  bot.sendMessage(me, message);
}

function photoMe (photo) {
  justSended = true;
  bot.sendPhoto(me, photo);
}

redisSub.subscribe('itemPrice');
redisSub.subscribe('captcha');

redisSub.on(
  "message", function (channel, message) {
    message = JSON.parse(message);
    console.log(channel, message);

    switch ( channel ) {
      case "itemPrice":
        say(message.name + "\nprice: $" + message.price + "\ndiscount: " + message.discount + "% \nurl: " + message.url);
        break;
      case "captcha":
        photoMe(message.path);
        break
    }
  }
);