var ps          = require('../components/ps');
var redisClient = require('../components/redis');
var socket      = require('../components/socket');

module.exports = function (id, cb) {
  ps.process(
    function () {
      ps.lock();
      redisClient.srem(
        'held', id, function () {
          socket.emit('released', { id: id });
          ps.unlock();
          if ( cb ) {
            cb();
          }
        }
      );
    }
  );
};