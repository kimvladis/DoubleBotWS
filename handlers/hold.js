var ps          = require('../components/ps');
var redisClient = require('../components/redis');
var socket      = require('../components/socket');

module.exports = function (id, cb) {
  ps.process(
    function () {
      ps.lock();
      redisClient.sadd(
        'held', id, function () {
          socket.emit('held', { ids: [ id ] });
          ps.unlock();
          if ( cb ) {
            cb();
          }
        }
      );
    }
  );
};
