var
  express    = require('express'),
  http       = require('http'),
  https = require('https'),
  redis      = require("redis"),
  ejs        = require('ejs'),
  bodyParser = require('body-parser'),
  fs         = require("fs");

var privateKey = fs.readFileSync('/etc/letsencrypt/live/vladislav.kim/privkey.pem').toString();
var certificate = fs.readFileSync('/etc/letsencrypt/live/vladislav.kim/cert.pem').toString();

var credentials = {key: privateKey, cert: certificate};

global.lastWithdraw = { time: 0, sum: 0 };

var
  io       = require('./components/socket'),
  ps       = require('./components/ps.js'),
  db       = require('./components/db'),
  redisSub = require('./components/redisSub');

var app = express();

app.set('views', __dirname + '/views');
app.engine('html', ejs.renderFile);

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded(
    {
      extended: true
    }
  )
);

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

var socket = io.listen(httpsServer);

httpServer.listen(80);
httpsServer.listen(443);

app.use('/bot/captcha', require('./controllers/captcha'));
app.use('/bot/list', require('./controllers/list'));
app.use('/bot', require('./controllers/bot'));
app.use('/', require('./controllers/bot'));
app.use('/bot/status', require('./controllers/status'));
app.use('/bot', express.static('public'));
app.use('/.well-known', express.static('.well-known'));
app.use('/bot/logs', express.static('logs'));