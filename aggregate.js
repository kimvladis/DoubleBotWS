db.items.aggregate(
  [
    {
      $match: {
        created_at: {
          $gte: ISODate("2016-07-25T00:00:00.000Z")
        }
      }
    },
    {
      $group: {
        _id: {
          hour: {
            $hour: "$created_at"
          }
        },
        count: {
          $sum: 1
        }
      }
    },
    {
      $sort: {
        count: -1
      }
    }
  ]
);
