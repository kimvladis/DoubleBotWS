var Nightcrawler = require('nightcrawler'),
    request      = require('request'),
    req          = request.defaults({ jar: true, json: true }),
    crypto       = require('crypto'),
    redis        = require("redis"),
    captcha      = require('./components/ruCaptcha'),
    captchas     = [],
    lastUpdate   = 0,
    mongoose     = require('mongoose'),
    profiler     = require('./components/profiler'),
    botCount     = 0,
    unique       = require('./components/unique'),
    logger       = require('./components/logger');

var torLogger   = logger.getTorLogger;

var Item        = require('./models/item.js');
var Withdraw    = require('./models/withdraw.js');
var WithdrawAll = require('./models/withdrawAll.js');

mongoose.connect('mongodb://localhost/test');

var nightcrawler = new Nightcrawler(
  {
    // Your Tor-Backed HTTP proxy URL
    proxy: 'http://localhost:8123',
    // Tor's control interface port and password
    tor:   {
      password:    'qawsedrf',
      controlPort: 9051
    }
  }
);

var redisClient = redis.createClient();
var redisSub    = redis.createClient();
var redisPub    = redis.createClient();
redisPub.publish('reqBotCount', true);
redisSub.on(
  "message", function (channel, message) {
    switch ( channel ) {
      case "captcha":
        torLogger.trace('Capthca: ' + message);
        captchas.push(message);
        break;
      case "botCount":
        message  = JSON.parse(message);
        botCount = message;
        torLogger.trace('Bots count: ' + botCount);
        break;
    }
  }
);

redisSub.subscribe("captcha");
redisSub.subscribe("botCount");

var j      = request.jar();
var cookie = request.cookie('PHPSESSID=nutpfbb242vle854c6ij69tdd6;');
var url    = 'https://csgopolygon.com/withdraw.php';
j.setCookie(cookie, url);


//curl 'http://csgopolygon.com/scripts/_get_bank.php?undefined'
//-H 'Cookie: PHPSESSID=pb9pv7cr6jkpf8mv305k76lf06; language=en'
//-H 'Accept-Encoding: gzip, deflate, sdch'
//-H 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
//-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106
// Safari/537.36'  -H 'Accept: */*'  -H 'Referer: http://csgopolygon.com/withdraw.php'  -H 'X-Requested-With:
// XMLHttpRequest'  -H 'Connection: keep-alive'  -H 'Cache-Control: max-age=0' --compressed
var options = {
  headers: {
    //'Accept-Encoding': 'gzip, deflate, sdch',
    'Accept-Language':  'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
    'User-Agent':       'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    'Accept':           '*/*',
    'Referer':          'https://csgopolygon.com/withdraw.php',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection':       'keep-alive',
    'Cache-Control':    'max-age=0'
  },
  //proxy:   nightcrawler.proxy,
  jar:     j
};

function checkInList (items) {
  return new Promise(function (resolve, reject) {
      redisClient.get(
        'list',
        function (err, data) {
          data               = JSON.parse(data);
          var necesseryItems = data.items;

          var _items = items.filter(
            function (item) {
              return necesseryItems.indexOf(item.name) >= 0;
            }
          );

          resolve(_items);
        }
      );
    }
  );
}

function checkInWithdraw (items) {
  return new Promise(function (resolve, reject) {
      Withdraw.find(
        {},
        function (err, withdraws) {
          var withdrawed = withdraws.reduce(
            function (prev, curr) {
              return prev.concat(curr.assetids);
            }, []
          ).unique();

          items = items.filter(
            function (item) {
              return withdrawed.indexOf(item.assetid) < 0;
            }
          )

          resolve(items);
        }
      );
    }
  );
}

function sendItems (items) {
  return new Promise(function (resolve, reject) {
      var botIds = items.map(
        function (item) {
          return item.botid;
        }
      ).unique();

      botIds.forEach(
        function (botid) {
          var botItems = items.filter(
            function (item) {
              return item.botid === botid;
            }
          );

          botItems = botItems.slice(0, 20);

          var response = {
            items: botItems.reduce(
              function (prev, curr) {
                return prev + curr.assetid + ',';
              }, ''
            ),
            sum:   botItems.reduce(
              function (prev, curr) {
                return prev + curr.price;
              }, 0
            ),
            botid: botid
          };
          if ( botCount > 0 ) {
            var obj      = {
              assetids:  botItems.map(
                function (T) {
                  return T.assetid;
                }
              ),
              sum:       response.sum,
              createdAt: new Date(),
            };
            var withdraw = new Withdraw(obj);
            withdraw.save();
            var withdrawAll = new WithdrawAll(obj);
            withdrawAll.save();
          }
          botCount--;
          if ( botCount < 0 ) {
            botCount = 0;
          }
          redisPub.publish("withdraw", JSON.stringify(response));
          torLogger.trace('Withdraw ' + response.items + ' on sum ' + response.sum);
        }
      );
      resolve(botIds);
    }
  )
}

function checkItems (items) {
  checkInWithdraw(items)
    .then(checkInList)
    .then(sendItems);
}

function chart (type, y) {
  redisPub.publish('chart', JSON.stringify(
    {
      type: type,
      time: Math.floor(new Date() / 1000),
      y:    y
    }
                   )
  );
}

function refresh () {
  if ( botCount > 0 ) {
    torLogger.trace('Getting captcha');
    captcha.getOne(function (captcha) {
      torLogger.trace('Captcha - ' + captcha);
      options.url = 'https://csgopolygon.com/scripts/_get_bank.php?captcha=' + captcha;
      // console.log(options);
      profiler.start('reqWithdraw');
      try {
        req(
          options, function (err, res, body) {
            if ( body.success ) {
              var time = profiler.stop('reqWithdraw');
              chart('withdraw', time);
              torLogger.trace('api answered successfully. Time: %d', time);

              redisPub.publish(
                "balance",
                JSON.stringify(
                  {
                    balance: body.balance,
                    avail:   body.avail
                  }
                )
              );

              profiler.start('mongo');

              checkItems(body.items);

              Item.collection.insert(body.items, { ordered: false }, onInsert);

              function onInsert (err, docs) {
                if ( err ) {
                  if ( err.code !== 11000 ) {
                    torLogger.error(err.message);
                    err.writeErrors.forEach(
                      function (err) {
                        if ( err.code !== 11000 ) {
                          torLogger.error(err.message);
                        }
                      }
                    );
                  }
                }

                torLogger.trace('%d items were successfully stored. Time: %d ms', parseInt(docs.nInserted),
                             profiler.stop('mongo')
                );

                //Item.find({ name: { $in: necesseryItems }}, function (err, items) {
                //  checkItems(items);
                //});
              }

              setTimeout(
                function () {
                  refresh();
                }, 10000
              );
            } else {
              torLogger.warn(body);
              options.url = 'https://csgopolygon.com/banhammer/pid';
              req(options, function (err, res, body) {
                var cookie = request.cookie('BHC=' + body);
                var url    = 'https://csgopolygon.com/withdraw.php';
                options.jar.setCookie(cookie, url);
                refresh();
              });
            }
          }
        );
      } catch ( e ) {
        torLogger.trace(e);
      }
    });
  } else {
    setTimeout(refresh, 1000);
  }
}

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function refreshTest () {
  profiler.start('start');
  setTimeout(function () {
               chart('start', profiler.stop('start'));
               profiler.start('withdraw');
               setTimeout(function () {
                            chart('withdraw', profiler.stop('withdraw'));
                            refreshTest();
                          }, getRandomInt(500, 1500)
               );
             }, getRandomInt(100, 500)
  );
}

nightcrawler.changeIp().then(
  function (ip) {
    torLogger.trace('My current IP is: ' + ip);
    refresh();
  }
);

captcha.fillPull();

torLogger.trace('started');

captcha.checkBalance();

redisClient.get('balance', function (err, data) {
  redisPub.publish(
    "balance",
    data
  );
});
