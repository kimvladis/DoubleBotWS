var
  Horseman = require('node-horseman'),
  log4js   = require('log4js');

log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('logs/polygon.log'));

var logger = log4js.getLogger();

var horseman = new Horseman({
    timeout: 10000
  }
);

horseman
  .open('http://csgopolygon.com/rolls.php')
  .waitForSelector('.table')
  .evaluate(function () {
    return $('td a').map(function () {return $(this).attr('href'); }).get();
  })
  .then(function (urls) {
    function parse (urls, max, sum, count) {
      url = urls.pop();
      if (!url) {
        logger.trace('max - ' + max);
        logger.trace('avg - ' + sum/count);
        return;
      }

      //logger.trace(url)

      var _horseman = new Horseman({
          timeout: 10000
        }
      );

      _horseman
        .open('http://csgopolygon.com/rolls.php' + url)
        .evaluate( function () {
          return Math.max.apply(Math, $('.td-val').map(function() {return parseInt($(this).text());}).get().reduce(function(prev, curr) {
            var result;
            if (prev.length == 0) {
              prev.push(1);
            } else {
              if (curr != 0)
                prev.push(prev.pop()+1);
              else
                prev.push(1);
            }

            return prev;
          }, []));
        })
        .then(function (_max) {
          if (_max > max) {
            logger.trace(_max);
          }
          sum += _max;
          parse(urls, Math.max(_max, max), sum, count +1);
          _horseman.close();
        })

    }

    parse(urls, 0, 0, 0);
    horseman.close();
  });


